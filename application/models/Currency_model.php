<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_model extends CI_model {

	public function get_currency_rate(){
        $sql = "
        SELECT 
	        sell_price, buy_price
        FROM currency p 
		WHERE currency_from = ? AND currency_to = ?";
        return $this->db->query($sql, array("USD", "IDR"))->result_array()[0]['sell_price'];
    }
}
