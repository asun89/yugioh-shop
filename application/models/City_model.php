<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_model extends CI_model {

	public function get_list(){
        $sql = "
        SELECT 
	        id, CONCAT(name, ' - ', province) as name
        FROM cities";
        $cities = $this->db->query($sql)->result_array();

        return $cities;
    }
}
