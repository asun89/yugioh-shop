<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_model {

	public function insert($data){
		$sql = "
			INSERT INTO transactions 
			(total_price, total_discount, total_markup, shipping_fee, account_id, customer_title, 
			customer_fullname, customer_email, customer_phone, customer_address, status, created_by, updated_by)
			VALUES(?,?,?,?,?,?,?,?,?,?,?,1,1)
		";
		$this->db->query($sql, array(
			$data['total_price'],
			$data['total_discount'],
			$data['total_markup'],
			$data['shipping_fee'],
			$data['account_id'],
			$data['customer_title'],
			$data['customer_fullname'],
			$data['customer_email'],
			$data['customer_phone'],
			$data['customer_address'],
			$data['status']
		));

		return $this->db->insert_id();
	}

	public function update($data, $transaction_id){
		$sql = "
			UPDATE transactions 
			SET total_price = ?, total_discount = ?, total_markup = ?, account_id = ?, customer_title = ?, 
			customer_fullname = ?, customer_email = ?, customer_phone = ?, customer_address = ?, status = ?, created_by = 1, updated_by = 1
			WHERE id = ?
		";
		$this->db->query($sql, array(
			$data['total_price'],
			$data['total_discount'],
			$data['total_markup'],
			$data['account_id'],
			$data['customer_title'],
			$data['customer_fullname'],
			$data['customer_email'],
			$data['customer_phone'],
			$data['customer_address'],
			$data['status'],
			$transaction_id
		));
	}

	public function get_by_account_id_and_status($account_id, $status){
		$sql = "SELECT * FROM transactions WHERE account_id = ? AND status = ?";
		return $this->db->query($sql, array($account_id, $status))->result_array();
	}

	public function get_transaction_list_by_account_id_and_statuses_not_in($account_id, $statuses){
		$sql = "SELECT * FROM transactions WHERE account_id = ? AND status NOT IN (".implode(",", $statuses).")";
		return $this->db->query($sql, array($account_id))->result_array();
	}
	
	public function update_transaction_status($transaction_id, $status){
		$sql = "UPDATE transactions SET status = ? WHERE id = ?";
		$this->db->query($sql, array($status, $transaction_id));
	}
}
