<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_detail_model extends CI_model {

	public function insert($data){
		$sql = "
		INSERT INTO transaction_details
		(transaction_id, product_id, qty, price, subtotal)
		VALUES(?,?,?,?,?)";
		$this->db->query($sql, array(
			$data['transaction_id'],
			$data['product_id'],
			$data['qty'],
			$data['price'],
			$data['subtotal']
		)
		);

		return $this->db->insert_id();
	}

	public function get_transaction_detail_by_account_and_product_id($transaction_id, $product_id){
		$sql = "
			SELECT td.id as td_id, td.price, td.qty, td.subtotal FROM transaction_details td
			WHERE transaction_id = ? AND product_id = ?
		";

		$td = $this->db->query($sql, array($transaction_id, $product_id))->result_array();
		if(count($td) > 0){
			return $td[0];
		}
		return false;
	}


	public function update_transaction_detail_data_qty_and_subtotal($data, $transaction_detail_id){
		$sql = "
			UPDATE transaction_details
			SET qty = ?,
			price = ?,
			subtotal = ?
			WHERE id = ?
		";

		$this->db->query($sql, array($data['qty'], $data['price'], $data['subtotal'], $transaction_detail_id));

		return $transaction_detail_id;
	}

	public function get_complete_transaction_detail_by_account_id_and_status($account_id, $status){
		$sql = "
			SELECT 
				td.id,
				td.transaction_id,
				product_id,
				c.name,
				c.sku,
				c.rarity,
				c.metadata_json,
				td.qty,
				td.price,
				p.currency,
				subtotal,
				total_price,
				shipping_fee
			FROM 	
				transaction_details td
			JOIN 
				transactions t
			ON t.id = td.transaction_id
			JOIN 
				products p
			ON p.id = td.product_id
			JOIN 
				cards c
			ON c.reference_id = p.card_reference_id
			WHERE account_id = ? AND status = ?
		";

		return $this->db->query($sql, array($account_id, $status))->result_array();
	}

	public function get_transaction_details_by_transaction_id($transaction_id){
		$sql = "
			SELECT * FROM transaction_details WHERE transaction_id = ?
		";

		return $this->db->query($sql, array($transaction_id))->result_array();
	}
}
