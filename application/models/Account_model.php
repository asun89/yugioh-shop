<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_model {

	public function get_active_account_by_username_and_password($username, $password){
        $sql = "
        SELECT 
	        id, username, fullname, email, phone, address, city_id
        FROM accounts 
		WHERE username = ? AND password = ? AND active = 1";
        $accounts = $this->db->query($sql, array($username, $password))->result_array();

        if(count($accounts) > 0){
            return $accounts[0];
        } else {
            return false;
        }
    }

    public function get_active_account_by_username($username){
        $sql = "
        SELECT 
	        id
        FROM accounts 
		WHERE username = ? AND active = 1";
        $accounts = $this->db->query($sql, array($username))->result_array();

        if(count($accounts) > 0){
            return $accounts[0];
        } else {
            return false;
        }
    }

    public function get_account_by_id($id){
        $sql = "
        SELECT 
	        id, username, fullname, email, phone, address, password, city_id
        FROM accounts 
		WHERE id = ?
        ";

        $accounts = $this->db->query($sql, array($id))->result_array();

        if(count($accounts) > 0){
            return $accounts[0];
        } else {
            return false;
        }
    }

    public function update_account_by_id($account, $id){
        $sql = "
        UPDATE accounts 
        SET 
            password = ?, fullname = ?, email = ?, phone = ?, address = ?, city_id = ?
        WHERE 
            id = ?";
        $this->db->query($sql, 
        array(
            $account['password'], 
            $account['fullname'],
            $account['email'], 
            $account['phone'],
            $account['address'],
            $account['city'],
            $id
        ));
    }

    public function insert($account){
        $sql = "
        INSERT INTO accounts 
        (username, password, fullname, email, phone, address, city_id, active, created_by, updated_by, created_datetime, updated_datetime)
        VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, 
        array(
            $account['username'], 
            $account['password'], 
            $account['fullname'],
            $account['email'], 
            $account['phone'],
            $account['address'],
            $account['city'],
            $account['active'],
            $account['created_by'],
            $account['updated_by'],
            $account['created_datetime'],
            $account['updated_datetime']
        ));
    }
}
