<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scrapping_model extends CI_model {

    public function __construct()
    {
            parent::__construct();
            $this->load->helper("simple_html_dom");
    }
    
	public function get_description_by_card_name($name){
        $html = file_get_html("https://yugipedia.com/wiki/".str_replace(" ", "_", $name));
        foreach($html->find('.lore') as $element){
            return trim(preg_replace("/[\\n\\r]+/", "", $element->plaintext));
        }

        return "";
    }

    private function tdrows($elements)
    {
        $str = "";
        foreach ($elements as $element) {
            $str .= $element->nodeValue . ", ";
        }

        return $str;
    }

    public function get_card_rarity_by_name_and_sku($name, $sku){
        $yugioh_page = file_get_contents("https://yugipedia.com/wiki/".str_replace(" ", "_", $name));
        preg_match_all('/<table id="cts--EN" class="wikitable sortable card-list cts">(.*)<\/table>/', $yugioh_page, $output_array);

        $DOM = new DOMDocument;
        $DOM->loadHTML($output_array[0][0]);

        $items = $DOM->getElementsByTagName('tr');

        foreach ($items as $node) {
            $split = explode(",", $this->tdrows($node->childNodes));
            if(strtoupper(trim($split[1])) == strtoupper(trim($sku))){
                return $split[3];
            }
        }
    }

    public function get_card_price_by_sku_from_tnt($sku_rarity){
        $yugioh_page = file_get_contents("https://www.trollandtoad.com");
        preg_match_all('/name=\'token\' value=\'(.*)\'>/', $yugioh_page, $output_array);
        $html = file_get_html("https://www.trollandtoad.com/category.php?token=".$output_array[1][0]."&search-words=".$sku_rarity."&selected-cat=0");
        $output = "";
        foreach($html->find('.buying-options-table') as $element){
            $output = trim(preg_replace("/[\\n\\r]+/", "", $element->plaintext));
            break;
        }
        $data = explode("$", $output);
        $price_data = explode(" ", $data[1]);
        
        return $price_data[0];
    }

    public function get_all_ygoprodeck_data(){
        $response = file_get_contents("https://db.ygoprodeck.com/api/v7/cardinfo.php");
        return json_decode($response, true);
    }

    public function search_tokopedia(){
        $json_payload = '[{"operationName":"SearchProductQueryV4","variables":{"params":"device=desktop&navsource=&ob=23&page=1&q=utopia%20yugioh&related=true&rows=60&safe_search=false&scheme=https&shipping=&source=search&srp_component_id=02.01.00.00&srp_page_id=&srp_page_title=&st=product&start=0&topads_bucket=true&unique_id=33846e8fe1d80122eda5374f85ee0e96&user_addressId=175719472&user_cityId=146&user_districtId=1632&user_id=11191847&user_lat=-6.196537999999999&user_long=106.6593494&user_postCode=15141&user_warehouseId=13209539&variants="},"query":"query SearchProductQueryV4($params: String!) {  ace_search_product_v4(params: $params) {    header {      totalData      totalDataText      processTime      responseCode      errorMessage      additionalParams      keywordProcess      componentId      __typename    }    data {      banner {        position        text        imageUrl        url        componentId        trackingOption        __typename      }      backendFilters      isQuerySafe      ticker {        text        query        typeId        componentId        trackingOption        __typename      }      redirection {        redirectUrl        departmentId        __typename      }      related {        position        trackingOption        relatedKeyword        otherRelated {          keyword          url          product {            id            name            price            imageUrl            rating            countReview            url            priceStr            wishlist            shop {              city              isOfficial              isPowerBadge              __typename            }            ads {              adsId: id              productClickUrl              productWishlistUrl              shopClickUrl              productViewUrl              __typename            }            badges {              title              imageUrl              show              __typename            }            ratingAverage            labelGroups {              position              type              title              url              __typename            }            componentId            __typename          }          componentId          __typename        }        __typename      }      suggestion {        currentKeyword        suggestion        suggestionCount        instead        insteadCount        query        text        componentId        trackingOption        __typename      }      products {        id        name        ads {          adsId: id          productClickUrl          productWishlistUrl          productViewUrl          __typename        }        badges {          title          imageUrl          show          __typename        }        category: departmentId        categoryBreadcrumb        categoryId        categoryName        countReview        customVideoURL        discountPercentage        gaKey        imageUrl        labelGroups {          position          title          type          url          __typename        }        originalPrice        price        priceRange        rating        ratingAverage        shop {          shopId: id          name          url          city          isOfficial          isPowerBadge          __typename        }        url        wishlist        sourceEngine: source_engine        __typename      }      violation {        headerText        descriptionText        imageURL        ctaURL        ctaApplink        buttonText        buttonType        __typename      }      __typename    }    __typename  }}"}]';
        $make_call = $this->postProcess('https://gql.tokopedia.com/graphql/SearchProductQueryV4', $json_payload);
        $response = json_decode($make_call, true);

        print_r($response);
    }

    public function postProcess($url, $data){
        $options = array('https' =>
            array(
                'method'  => 'POST',
                'header' => "Accept-Encoding: gzip, deflate, br\r\n" ,
                            "Accept: application/json\r\n" ,
                            "Content-type: application/json\r\n" .
                            "Content-length: " . strlen($data) . "\r\n" ,
                            "Cookie: _abck=9AAB90751944475CAB26400084E3DFA3~-1~YAAQT+UcuKE2mFuEAQAAHuy4egg+ecvNdVTRmMphCQ6GOQHuZ/a+at5DpMcWl1xhLpSPiE9Qyff31vEvZvrB3Y1qHc3WHQS6EO3+xfS3zLYx0PONydgy6pTaUIYOKdPlPMRyHz2jhfYCzvDTkceV3fYP3PMXyVhsqdSRMVIr0a1qv/sdWOc6sbq80oP72QMHcR8P8JZEsyDWr3gekPoYGvt2BDHtH4xGPK8xPUbOFTWW9JFut1MceCaK6XO/fnAmAF8ZDjTicfRQHJRnHrjAH4Q+O/JAij66uaMy5T3PphHy5531PxvxeTNNbTL1ynhLbw0cJRrjYhacDFOEFtSgZauiAtWRiaTih1DsWp7a48NnqThfPWfY8WDXG3WuKha4PUL5NqepEFKlYw==~-1~-1~-1; bm_sz=31019482F33B56675EFE59723B7DC0E7~YAAQT+UcuKI2mFuEAQAAHuy4ehHfb3QKe+R9xQanTsB8nt+JgqKYxKeqqka/fSfI1fVg6CQjOb/CvpdvYhFkLT93DDxwUeyXb5/8VadI+KsgRpsGIwSnVYYjMlHSB6lbLjx6w6AhD64wlDJpcKlqoIYPrs3InlJVO1v/F/dxQWlk2a+KjSr7YBqGzx4Ntqo89eTFVsf3bpUXUqzBBnFHSvLKArFo1IkTZcxbYl7BLItOACVXuYt258kTeUP63D4hX4JcnR3G/Tw1ekbRr5fkbC/OWqR3lGUC4iC6Ir93t1IDCseZQzU=~3360323~4339249\r\n",
                'content' => $data
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($url,false,$context);
        
        return $result;
     }
}
