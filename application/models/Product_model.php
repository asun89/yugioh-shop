<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_model {

	public function get_products_by_ids($ids = array()){
		$sql = "
			SELECT id, qty FROM products WHERE id IN (".implode(",", $ids).")
		";

		return $this->db->query($sql)->result_array();
	}

	public function get_products($search, $start, $limit){
		$where = '';
		if(!empty($search)){
			$where = "AND c.name LIKE '%". $search. "%'";
		}

        $sql = "
		SELECT 
			*,
			p.currency as product_currency,
			p.price as product_price,
			p.id as product_id
		FROM products p 
		JOIN cards c ON c.reference_id = p.card_reference_id 
		WHERE p.qty > 0 
		".$where."
		LIMIT ?, ?";
        return $this->db->query($sql, array($start, $limit))->result_array();
	}

	public function get_count_products($search){
		$where = '';
		if(!empty($search)){
			$where = "AND c.name LIKE '%". $search. "%'";
		}

        $sql = "
		SELECT 
			count(p.id) as count
		FROM products p 
		JOIN cards c ON c.reference_id = p.card_reference_id 
		WHERE p.qty > 0 
		".$where;
        return $this->db->query($sql)->row_array()['count'];
	}

	public function update_description_by_id($id, $description){
		$sql = "
			UPDATE 
				products
			SET
				description = ?
			WHERE
				id = ?
		";

		$this->db->query($sql, array($description, $id));
	}
	
	public function get_product_detail_by_id($id){
        $sql = "
        SELECT 
		*,
		p.currency as product_currency,
		p.price as product_price,
		p.id as product_id

		FROM products p 
		JOIN cards c ON c.reference_id = p.card_reference_id 
		WHERE p.id = ?";

		$products = $this->db->query($sql, array($id))->result_array();

		if(count($products) > 0){
			return $products[0];
		} 
		return false;
	}

    public function search_products($words){
        $sql = "
        SELECT 
	        p.id as product_id, 
	        p.price, 
	        p.sku, 
	        p.description, 
	        p.currency, 
	        p.qty, 
	        p.name, 
	        p.image,
	        c.name as category_name, 
	        c.value as category_value, 
	        c.caption as category_caption 
        FROM products p 
        JOIN categories c ON p.id = c.product_id
        WHERE p.name LIKE '%$words%'
		ORDER BY p.updated_datetime DESC
        ";
        return $this->db->query($sql)->result_array();
	}
	

	public function update_product_batch($update_queries){
		$this->db->update_batch('products', $update_queries, 'id');
	}
}
