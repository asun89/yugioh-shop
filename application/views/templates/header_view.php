<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.modal.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
    <title>Yu-Gi-Oh! Shop - Platform Jual Beli Kartu Yu-Gi-Oh! TCG - Pasti Original!</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<?php echo site_url('home');?>">Yugioh Card Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link" href="#" onclick="about()">Tentang</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#" onclick="how_to_buy()">Cara Bertransaksi</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#" onclick="contact_us()">Hubungi Kami</a>
            </li>
            <li class="nav-item" id="login_section">
                <a class="nav-link" href="#" id="login" onclick="login_form()">Login / Register</a>
            </li>
            <li class="nav-item" id="transaction_section">
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" id="search_field" type="text" placeholder="Search">
            <button class="btn btn-secondary my-2 my-sm-0" onclick="search_process(); return false;" type="submit">Search</button>
        </form>
        </div>
    </nav>