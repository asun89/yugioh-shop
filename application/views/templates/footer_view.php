    <div class="footer overflow-hidden col-md-12">
        <div class="float-left footer-left">&copy 2020 - <a href="http://yugioh.socianovation.com">Yugioh Card Shop</a></div>
        <div class="float-right footer-right">Powered By <a href="http://www.socianovation.com">SOCIANOVATION</a></div>
    </div>

    <div class="modal">
        <p>Loading Data...</p>
    </div>
	  
	</div>

    
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.x-git.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.modal.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>