<?php $this->load->view('templates/header_view'); ?>
    <div class="jumbotron nomargin">
        <div class="col-md-12 overflow-hidden" id="product_list">
        </div>
        <div id="pagination_container">
            <nav aria-label="Page Navigation">
              <ul class="pagination" id="product_paging">
              </ul>
            </nav>
        </div>
    </div>
<?php $this->load->view('templates/footer_view'); ?>