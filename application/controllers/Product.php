<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function list()
	{
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		header("Content-Type: application/json");
		header("Accept: application/json");

		$data = [];
		$maxwords = 20;
		$per_page = 12;
		$this->load->model('product_model');

		$search = $this->input->get("search");
		if(!isset($search)){
			$search = '';
		}

		$start = $this->input->get("start");
		if(empty($start)){
			$start = 0;
		}
		$start += 0;

		$limit = $this->input->get("limit");
		if(empty($limit)){
			$limit = $per_page;
		}
		$limit += 0;

		$products = $this->product_model->get_products($search, $start, $limit);
		$products_count = $this->product_model->get_count_products($search);
		$page_count = ceil($products_count / $per_page);

		$product_output = array();
		$product_output['list'] = array();
		$product_output['pages'] = array();
		foreach($products as $p){
			$metadata = json_decode($p['metadata_json'], true);
			$p['image'] = $metadata['card_images'][0]['image_url_small'];
			$p['image_high_res'] = $metadata['card_images'][0]['image_url'];
			$lenname = strlen($p['name']);
			$p['name'] = substr($p['name'], 0, $maxwords);
			$p['price'] = number_format($p['price'], 2, '.', ',');
			if($lenname > $maxwords){
				$p['name'] .= "...";
			}
			$p['description'] = $metadata['desc'];
			$product_output['list'][] = $p;
		}

		for($a = 0; $a < $page_count; $a++){
			$product_output['pages'][] = array(
				"start" => ($a * $per_page),
				"limit" => $per_page
			);
		}


		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = $product_output;
		$data['time'] = date("Y-m-d H:i:s");

		echo json_encode($data);
	}

	public function detail($id){
		if(!isset($id)){
			$data['code'] = "SUCCESS";
			$data['message'] = "Success";
			$data['data'] = array();
			$data['time'] = date("Y-m-d H:i:s");

			echo json_encode($data);
			return false;
		}
		$this->load->model('product_model');

		$product = $this->product_model->get_product_detail_by_id($id);

		$product_array = array();
		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['time'] = date("Y-m-d H:i:s");
		if($product) {
			$metadata = json_decode($product['metadata_json'], true);
			$product_array['product_id'] = $product['product_id'];
			$product_array['price'] = number_format($product['price'], 2, '.', ',');
			$product_array['sku'] = $product['sku'];
			$product_array['currency'] = $product['product_currency'];
			$product_array['qty'] = $product['qty'];
			$product_array['name'] = $product['name'];
			$product_array['description'] = $metadata['desc'];
			$product_array['image'] = $metadata['card_images'][0]['image_url'];

			$product_array['categories'] = array();
			$data['data'] = $product_array;
		} else {
			$data['code'] = "ERROR";
			$data['message'] = "Product not found";
			$data['data'] = array();
		}

		

		echo json_encode($data);
	}

	public function test_get(){
		$this->load->model('scrapping_model');
		$price = $this->scrapping_model->get_card_price_by_sku_from_tnt("ROTD-EN001");
	}
}
