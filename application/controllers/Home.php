<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = array(
			
		);
		$this->load->view('home/index_view', $data);
	}

	public function check_session(){
		$this->load->library('session');
		$session = $this->session->userdata();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = $session;
		$data['time'] = date("Y-m-d H:i:s");

		echo json_encode($data);
	}

	public function my_account(){
		$this->load->library('session');
		$session = $this->session->userdata();

		$this->load->model('transaction_detail_model');
		$this->load->model('account_model');

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['time'] = date("Y-m-d H:i:s");

		if(!isset($session['username'])){
			$data['code'] = "ERROR";
			$data['message'] = "Not Authorized, must login first";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$carts = $this->transaction_detail_model->get_complete_transaction_detail_by_account_id_and_status($session['id'], 1);

		$count_trans_detail = count($carts);
		for($a = 0; $a < $count_trans_detail; $a++){
			$carts[$a]['subtotal'] = $carts[$a]['currency']. " " . number_format($carts[$a]['subtotal'], 2);
			$carts[$a]['total_price'] = $carts[$a]['currency']. " " . number_format($carts[$a]['total_price'], 2);
			$carts[$a]['shipping_fee'] = $carts[$a]['currency']. " " . number_format($carts[$a]['shipping_fee'], 2);
		}

		$checkouts = $this->transaction_detail_model->get_complete_transaction_detail_by_account_id_and_status($session['id'], 2);

		$count_trans_detail = count($checkouts);
		for($a = 0; $a < $count_trans_detail; $a++){
			$checkouts[$a]['subtotal'] = $checkouts[$a]['currency']. " " . number_format($checkouts[$a]['subtotal'], 2);
			$checkouts[$a]['total_price'] = $checkouts[$a]['currency']. " " . number_format($checkouts[$a]['total_price'], 2);
			$checkouts[$a]['shipping_fee'] = $checkouts[$a]['currency']. " " . number_format($checkouts[$a]['shipping_fee'], 2);
		}

		$account = $this->account_model->get_account_by_id($session['id']);

		$output = array();
		$output['account'] = $account;
		$output['carts'] = $carts;
		$output['checkouts'] = $checkouts;
		$data['data'] = $output;

		echo json_encode($data);
	}

	public function logout(){
		$this->load->library('session');
		$this->session->sess_destroy();

		$data['code'] = "SUCCESS";
		$data['message'] = "Berhasil logout";
		$data['data'] = array();
		$data['time'] = date("Y-m-d H:i:s");

		echo json_encode($data);
	}

	public function login(){
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|htmlspecialchars');

		if ($this->form_validation->run() == FALSE) {
			$data['code'] = "ERROR";
			$data['message'] = validation_errors();
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$post = $this->input->post();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = array();
		$data['time'] = date("Y-m-d H:i:s");
		

		if(isset($post['username']) && isset($post['password'])){
			$this->load->model('account_model');
			$account = $this->account_model->get_active_account_by_username_and_password($post['username'], md5($post['password']));
			if($account){
				$this->load->library('session');
				$this->session->set_userdata($account);
				$data['message'] = "Login Sukses";
				$data['data'] = array();
			} else {
				$data['code'] = "ERROR";
				$data['message'] = "Username tidak ditemukan";
			}
		} else {
			$data['code'] = "ERROR";
			$data['message'] = "Username dan Password wajib diisi";
		}

		echo json_encode($data);
	}

	public function register(){
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('fullname', 'Fullname', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|htmlspecialchars');
		$this->form_validation->set_rules('phone', 'Nomor Telepon', 'required|numeric|trim|htmlspecialchars');
		$this->form_validation->set_rules('address', 'Alamat', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('city', 'Kota', 'required|trim|htmlspecialchars');

		if ($this->form_validation->run() == FALSE) {
			$data['code'] = "ERROR";
			$data['message'] = validation_errors();
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$post = $this->input->post();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = array();
		$data['time'] = date("Y-m-d H:i:s");
		
		$this->load->model('account_model');
		$account = $this->account_model->get_active_account_by_username($post['username']);
		if($account){
			$data['code'] = "ERROR";
			$data['message'] = "Username telah terpakai";
		} else {
			$new_account = $post;
			$new_account['password'] = md5($post['password']);
			$new_account['created_by'] = 1;
			$new_account['updated_by'] = 1;
			$new_account['created_datetime'] = date("Y-m-d H:i:s");
			$new_account['updated_datetime'] = date("Y-m-d H:i:s");
			$new_account['active'] = 1;
			$this->account_model->insert($new_account);
			$data['message'] = "Registrasi berhasil";
		}

		echo json_encode($data);
	}

	public function update_account(){
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', 'Password', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('fullname', 'Fullname', 'required|trim|htmlspecialchars');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|htmlspecialchars');
		$this->form_validation->set_rules('phone', 'Phone', 'required|numeric|trim|htmlspecialchars');
		$this->form_validation->set_rules('address', 'Address', 'required|trim|htmlspecialchars');

		if ($this->form_validation->run() == FALSE) {
			$data['code'] = "ERROR";
			$data['message'] = validation_errors();
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$this->load->library('session');
		$session = $this->session->userdata();
		$this->load->model('account_model');

		$post = $this->input->post();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['time'] = date("Y-m-d H:i:s");

		if(!isset($session['username'])){
			$data['code'] = "ERROR";
			$data['message'] = "Not Authorized, must login first";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$account = array(
			"fullname" => $post['fullname'],
			"email" => $post['email'],
			"phone" => $post['phone'],
			"address" => $post['address']
		);

		$existing_account = $this->account_model->get_account_by_id($session['id']);
		if(!$existing_account) {
			$data['code'] = "ERROR";
			$data['message'] = "Akun tidak ditemukan";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$account["password"] = $existing_account['password'];
		if($existing_account['password'] != $post['password']){
			$account["password"] = md5($post['password']);
		}

		$this->account_model->update_account_by_id($account, $session['id']);

		$output = array();
		$output['account'] = $account;
		$data['data'] = $output;

		echo json_encode($data);
	}

	public function tokopedia_check(){
		$this->load->model('scrapping_model');
		print_r($this->scrapping_model->search_tokopedia());
	}
}
