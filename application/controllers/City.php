<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {

	public function get_list(){

		$this->load->model('city_model');
		$cities = $this->city_model->get_list();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = $cities;
		$data['time'] = date("Y-m-d H:i:s");

		echo json_encode($data);
	}
}
