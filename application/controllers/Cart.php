<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function add_to_cart(){
		$this->load->library('session');
		$this->load->model('transaction_model');
		$this->load->model('account_model');
		$session = $this->session->userdata();

		if(!isset($session['username'])){
			$data['code'] = "ERROR";
			$data['message'] = "Anda belum login, harap login dahulu";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}
		$account_id = $session['id'];

		$cart_array = $this->transaction_model->get_by_account_id_and_status($account_id, 2);
		if(count($cart_array) > 0){
			$data['code'] = "ERROR";
			$data['message'] = "Terdapat transaksi yang masih aktif dan belum terbayar, mohon menyelesaikan pembayaran atau lakukan 'Batalkan Pembelian'";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$post = $this->input->post();

		$output = array();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['data'] = $output;
		$data['time'] = date("Y-m-d H:i:s");

		if(isset($post['qty']) && !empty($post['qty']) && is_numeric($post['qty'])){
			$qty = $post['qty'];
		} else {
			$data['code'] = "ERROR";
			$data['message'] = "Qty harus berupa angka";
			echo json_encode($data);
			return false;
		}

		if(isset($post['product_id']) && !empty($post['product_id']) && is_numeric($post['product_id'])){
			$product_id = $post['product_id'];
			$this->load->model('product_model');
			$product = $this->product_model->get_product_detail_by_id($product_id);

			if($product['qty'] < $qty){
				$data['code'] = "ERROR";
				$data['message'] = "Stok produk tidak cukup";
				echo json_encode($data);
				return false;
			}
		} else {
			$data['code'] = "ERROR";
			$data['message'] = "Produk tidak dapat ditemukan";
			echo json_encode($data);
			return false;
		}

		$shipping_fee = 10000;
		$account = $this->account_model->get_account_by_id($account_id);
		$cart_array = $this->transaction_model->get_by_account_id_and_status($account_id, 1);
		if(count($cart_array) <= 0){
			$cart_data = array();
			$cart_data['total_price'] = 0;
			$cart_data['total_discount'] = 0;
			$cart_data['total_markup'] = 0;
			$cart_data['shipping_fee'] = $shipping_fee;
			$cart_data['account_id'] = $account['id'];
			$cart_data['customer_title'] = "mr";
			$cart_data['customer_fullname'] = $account['fullname'];
			$cart_data['customer_email'] = $account['email'];
			$cart_data['customer_phone'] = $account['phone'];
			$cart_data['customer_address'] = $account['address'];
			$cart_data['status'] = 1;

			$transaction_id = $this->transaction_model->insert($cart_data);
			$cart_data['id'] = $transaction_id;
			$cart = $cart_data;
		} else {
			$cart = $cart_array[0];
			$transaction_id = $cart['id'];
		}

		$this->load->model('transaction_detail_model');
		$existing_transaction_detail = $this->transaction_detail_model->get_transaction_detail_by_account_and_product_id($transaction_id, $product_id);
		if($existing_transaction_detail){
			$cart_detail_data = $existing_transaction_detail;
			$cart_detail_data['qty'] = $cart_detail_data['qty'] + $qty;
			$cart_detail_data['price'] = $product['product_price'];
			$cart_detail_data['subtotal'] = $product['product_price'] * $cart_detail_data['qty'];

			$this->transaction_detail_model->update_transaction_detail_data_qty_and_subtotal($cart_detail_data, $existing_transaction_detail['td_id']);
		} else {
			$cart_detail_data = array();
			$cart_detail_data['transaction_id'] = $cart['id'];
			$cart_detail_data['product_id'] = $product_id;
			$cart_detail_data['qty'] = $qty;
			$cart_detail_data['price'] = $product['product_price'];
			$cart_detail_data['subtotal'] = $product['product_price'] * $qty;
			$transaction_detail_id = $this->transaction_detail_model->insert($cart_detail_data);
		}

		$cart_data = array();
		$cart_data['shipping_fee'] = $shipping_fee; //fixed for Jakarta to Jakarta
		$cart_data['total_price'] = $cart['total_price'] + $cart_detail_data['subtotal'] + $shipping_fee;
		$cart_data['total_discount'] = 0;
		$cart_data['total_markup'] = 0;
		$cart_data['account_id'] = $account['id'];
		$cart_data['customer_title'] = "mr";
		$cart_data['customer_fullname'] = $account['fullname'];
		$cart_data['customer_email'] = $account['email'];
		$cart_data['customer_phone'] = $account['phone'];
		$cart_data['customer_address'] = $account['address'];
		$cart_data['status'] = 1;
		$this->transaction_model->update($cart_data, $transaction_id);

		echo json_encode($data);
	}
}
