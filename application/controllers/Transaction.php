<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	public function update_status($status){
		$this->load->library('session');
		$session = $this->session->userdata();

		$post = $this->input->post();

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['time'] = date("Y-m-d H:i:s");

		if(!isset($session['username'])){
			$data['code'] = "ERROR";
			$data['message'] = "Anda belum login, harap login dahulu";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$account_id = $session['id'];

		$this->load->model('transaction_model');

		if($status == 7){
			$check_status = 2;
		} else {
			$check_status = $status - 1;
		}
		$cart_array = $this->transaction_model->get_by_account_id_and_status($account_id, $check_status);

		if(count($cart_array) > 0){
			$cart = $cart_array[0];
			$this->add_product_stock($cart['id'], $status);

			if(!$this->sub_product_stock($cart['id'], $status)){
				$data['code'] = "ERROR";
				$data['message'] = "Stok produk tidak cukup";
				$data['data'] = array();
				echo json_encode($data);
				return false;
			}

			$this->transaction_model->update_transaction_status($cart['id'], $status);

			if($status == '3'){
				$this->load->model('transaction_payment_model');
				$payment_data = array();
				$payment_data['transaction_id'] = $cart['id'];
				$payment_data['payment_method_id'] = 1;
				$payment_data['amount'] = $post['amount'];
				$payment_data['account_no'] = $post['account_no'];
				$payment_data['account_owner'] = $post['account_owner'];
				$payment_data['status'] = 0;
				$payment_data['created_by'] = 1;
				$payment_data['updated_by'] = 1;
				$this->transaction_payment_model->insert($payment_data);
			}
		}

		$data['data'] = array();
		echo json_encode($data);
	}

	public function index(){
		$this->load->library('session');
		$session = $this->session->userdata();

		$this->load->model('transaction_model');

		$data['code'] = "SUCCESS";
		$data['message'] = "Success";
		$data['time'] = date("Y-m-d H:i:s");

		if(!isset($session['username'])){
			$data['code'] = "ERROR";
			$data['message'] = "Anda belum login, harap login dahulu";
			$data['data'] = array();
			echo json_encode($data);
			return false;
		}

		$transactions = $this->transaction_model->get_transaction_list_by_account_id_and_statuses_not_in($session['id'], array(1,2));
		$count_transactions = count($transactions);

		$statuses = array(
			3 => 'Mengkonfirmasi..',
			4 => 'Terbayar',
			5 => 'Paket Dikirim',
			6 => 'Paket Diterima',
			7 => 'Dibatalkan'
		);

		for($a = 0; $a < $count_transactions; $a++){
			$transactions[$a]['transaction_status'] = 'Konfirmasi terkirim';
			if($statuses[$transactions[$a]['status']]){
				$transactions[$a]['transaction_status'] = $statuses[$transactions[$a]['status']];
			}
			$transactions[$a]['total_price'] = "IDR " . number_format($transactions[$a]['total_price'], 2);

			if(in_array($transactions[$a]['status'], array(5,6))){
				$transactions[$a]['transaction_status'] .= "<br/>";
				$transactions[$a]['transaction_status'] .= "Resi : <br/>".$transactions[$a]['customer_tracker_key'];
				$transactions[$a]['transaction_status'] .= "<br/>";
			} 
			unset($transactions[$a]['status']);
		}
		$output = array();
		$output['transactions'] = $transactions;
		$data['data'] = $output;

		echo json_encode($data);
	}
	
	private function sub_product_stock($transaction_id, $status){
		if($status == 2){
			$this->load->model('product_model');
			$this->load->model('transaction_detail_model');

			$transactions = $this->transaction_detail_model->get_transaction_details_by_transaction_id($transaction_id);

			$count_transactions = count($transactions);
			$ids = array();
			for($a = 0; $a < $count_transactions; $a++){
				$ids[] = $transactions[$a]['product_id'];
			}

			$products = $this->product_model->get_products_by_ids($ids);
			$product_map = array();	
			$products_count = count($products);
			for($a = 0; $a < $products_count; $a++){
				$product_map[$products[$a]['id']] = $products[$a]['qty'];
			}

			$update_queries = array();
			for($a = 0; $a < $count_transactions; $a++){
				if($product_map[$transactions[$a]['product_id']] < $transactions[$a]['qty']){
					return false;
				}

				$update_queries[] = array(
					"qty" => $product_map[$transactions[$a]['product_id']] - $transactions[$a]['qty'],
					"id" => $transactions[$a]['product_id']
				);
			}

			$this->product_model->update_product_batch($update_queries);

		}	
		return true;
	}

	private function add_product_stock($transaction_id, $status){
		if($status == 7){
			$this->load->model('product_model');
			$this->load->model('transaction_detail_model');

			$transactions = $this->transaction_detail_model->get_transaction_details_by_transaction_id($transaction_id);

			$count_transactions = count($transactions);
			$ids = array();
			for($a = 0; $a < $count_transactions; $a++){
				$ids[] = $transactions[$a]['product_id'];
			}

			$products = $this->product_model->get_products_by_ids($ids);
			$product_map = array();	
			$products_count = count($products);
			for($a = 0; $a < $products_count; $a++){
				$product_map[$products[$a]['id']] = $products[$a]['qty'];
			}

			$update_queries = array();
			for($a = 0; $a < $count_transactions; $a++){
				$update_queries[] = array(
					"qty" => $product_map[$transactions[$a]['product_id']] + $transactions[$a]['qty'],
					"id" => $transactions[$a]['product_id']
				);
			}

			$this->product_model->update_product_batch($update_queries);

		}	
		return true;
	}
}
