function money_format(currency, number){
  const formatter = new Intl.NumberFormat('en-ID', {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: 2
  });
  return formatter.format(number);
}

function check_session(){
  var responseText = $.ajax({url: "home/check_session", async : false}).responseText;
  var session = JSON.parse(responseText);
  if(typeof session.data.username !== "undefined"){
    return session.data.username;
  }
  return false;
}

function get_cities(){
  var responseText = $.ajax({url: "city/get_list", async : false}).responseText;
  var cities = JSON.parse(responseText).data;
  return cities;
}

function logout(){
  if (confirm("Anda yakin ingin logout?")) {
    var responseText = $.ajax({url: "home/logout", async : false}).responseText;
    var session = JSON.parse(responseText);
    if(session.code == "SUCCESS"){
      alert("Logout berhasil");
      document.location="";
      return true;
    }
    return false;
  } 
}

function login_form(){
  this.blur(); // Manually remove focus from clicked link.

  var html = "";
  html += '<div class="login-form">';
  html += '<form action="#" id="login-form">';
  html += '<div class="form-container col-md-12 login-title"><h4>Login / Register</h1></div>';
  html += '<div class="form-container col-md-12"><h6 style="text-align:center;">Sudah punya akun? Silahkan login</h6 ></div>';
  html += '<div class="form-container col-md-12">Username</div><div class="form-container col-md-12"><input type="text" class="form-control" name="username" /></div>';
  html += '<div class="form-container col-md-12">Password</div><div class="form-container col-md-12"><input type="password" class="form-control" name="password" /></div>';
  html += '<div class="form-container btn-input col-md-12"><input class="btn btn-primary" type="submit" value="Login" /></div>';
  html += '</form>';

  cities = get_cities();
  cities_combobox = '<select name="city" class="form-control" readonly>';
  for(var a = 0; a < cities.length; a++){
    var city = cities[a];
    cities_combobox += '<option value="'+city.id+'">'+city.name+'</option>';
  }
  cities_combobox += '</select>';

  html += '<form action="#" id="register-form">';
  html += '<div class="form-container col-md-12 register-caption"><h6 style="text-align:center;">Belum punya akun? Yuk daftar</h6 ></div>';
  html += '<div class="form-container col-md-12">Username</div><div class="form-container col-md-12"><input type="text" class="form-control" name="username" /></div>';
  html += '<div class="form-container col-md-12">Password</div><div class="form-container col-md-12"><input type="password" class="form-control" name="password" /></div>';
  html += '<div class="form-container col-md-12">Full Name</div><div class="form-container col-md-12"><input type="text" class="form-control" name="fullname" /></div>';
  html += '<div class="form-container col-md-12">Email</div><div class="form-container col-md-12"><input type="email" class="form-control" name="email" /></div>';
  html += '<div class="form-container col-md-12">Phone</div><div class="form-container col-md-12"><input type="text" class="form-control" name="phone" /></div>';
  html += '<div class="form-container col-md-12">Address</div><div class="form-container col-md-12"><textarea class="form-control" name="address"></textarea></div>';
  html += '<div class="form-container col-md-12">City</div><div class="form-container col-md-12">'+cities_combobox+'</div>';
  html += '<div class="form-container btn-input col-md-12"><input class="btn btn-primary" type="submit" value="Register" /></div>';
  html += '</form>';
  html += '</div>';

  $(".modal").html(html);
  $(".modal").modal();

  $('#login-form').submit(function(event){
    $.post("home/login", $(this).serialize(), function( data ) {
      var output = JSON.parse(data);
      alert(output.message);
      if(output.code == "SUCCESS"){
        document.location="";
      }
    });
    return false;
  });

  $('#register-form').submit(function(event){
    $.post("home/register", $(this).serialize(), function( data ) {
      var output = JSON.parse(data);
      alert(output.message);
      if(output.code == "SUCCESS"){
        document.location="";
      }
    });
    return false;
  });
  return false;
}

function confirm_payment_process(){
  
  var account_no = prompt("Masukkan nomor rekening pengirim : ", "");
  if(account_no.trim() == "" || isNaN(account_no)){
    alert("Nomor rekening harus berupa angka saja");
    $.modal.close();
    return false;
  }
  var account_owner = prompt("Masukkan nama pemilik rekening pengirim : ", "");
  if(account_owner.trim() == "" || !isNaN(account_owner)){
    alert("Nama Pemiliik rekening harus berupa huruf saja");
    $.modal.close();
    return false;
  }
  var value = prompt("Masukkan jumlah yang ditransfer : ", "0");
  if(value.trim() == "" || isNaN(value)){
    alert("Jumlah transfer harus berupa angka saja");
    $.modal.close();
    return false;
  }

  if (confirm("Apakah anda yakin telah memasukkan data pembayaran dengan benar?")) {
    var postData = {
      account_no : account_no,
      account_owner : account_owner,
      amount : value
    };
    change_to_checkout(3, postData);
    alert("Konfirmasi pembayaran telah kami terima, pesanan akan dikirim setelah selesai verifikasi dari kami");
    $.modal.close();
    return false;
  }

}

function cancel_order_process(){
  if (confirm("Apakah anda ingin membatalkan pesanan ini?")) {
    change_to_checkout(7);
    alert("Keranjang dapat digunakan kembali");
    search_process();
    $.modal.close();
  }

}

function checkout_process(){
  if (confirm("Data pengiriman dan kontak akan disesuaikan dengan data akun ini (mohon cek kembali untuk alamat yang tertera pada akun ini), apakah anda yakin ingin membeli seluruh barang di dalam keranjang ini?")) {
    
    if(change_to_checkout(2) == true){
      alert("Status keranjang berubah menjadi menunggu pembayaran, segera lakukan pembayaran untuk memastikan anda mendapat stok kartu ini");
    } else {
      alert("Gagal checkout");
    }
    search_process();
    $.modal.close();
  }

}

function change_to_checkout(status, postData){
  var responseText = $.ajax({url: "transaction/update_status/" + status, async : false, type : "post", data : postData}).responseText;
  var session = JSON.parse(responseText);
  if(session.code == "SUCCESS"){
    $.modal.close();
    return true;
  }
  return false;
}

function my_account(){
  this.blur(); // Manually remove focus from clicked link.

  var responseText = $.ajax({url: "home/my_account", async : false}).responseText;
  var my_account = JSON.parse(responseText);
  if(my_account.code == "ERROR"){
    alert("Not authorized, please login first");
    document.location = "";
    return false;
  }

  var html = "";

  //Waiting For Payment
  if(my_account.data.checkouts.length > 0){
    html += '<div class="cart">';
    html += '<form action="#" id="my-account">';
    html += '<div class="form-container col-md-12 cart_title"><h4>Menunggu Pembayaran</h1></div>';
    html += '<div class="form-container btn-input col-md-12"><input class="btn btn-primary" type="button" value="Konfirmasi Pembayaran" id="confirm_button" onclick="confirm_payment_process();" /> <input class="btn btn-primary" type="button" value="Batalkan Pembelian" id="cancel_button" onclick="cancel_order_process();" /></div>';
    html += '<div class="form-container col-md-12"><table class="cart_table">';
    html += '<thead><tr><td>No.</td><td>Card Name</td><td>Qty</td><td>Subtotal</td></tr></thead>';

      var checkouts = my_account.data.checkouts;
      var checkouts_count = checkouts.length;
      for(var a = 0; a < checkouts_count; a++){
        html += '<tbody><tr><td>'+(a + 1)+'</td><td>'+checkouts[a].name+'<br/>'+checkouts[a].sku+' - '+checkouts[a].rarity+'</td><td>'+checkouts[a].qty+'</td><td>'+checkouts[a].subtotal+'</td></tr></tbody>';
      }
      html += '<tbody><tr><td colspan="3">Ongkos Kirim : </td><td>'+checkouts[0].shipping_fee+'</td></tr></tbody>'
      html += '<tbody><tr><td colspan="3">Total : </td><td>'+checkouts[0].total_price+'</td></tr></tbody>'
    

    html += '</table></div>';
    html += '<div class="form-container btn-input col-md-12"></div>';
    html += '</form>';
    html += '</div>';
  }

  //Cart
  html += '<div class="cart">';
  html += '<form action="#" id="my-account">';
  html += '<div class="form-container col-md-12 cart_title"><h4>Keranjang</h1></div>';
  
  if(my_account.data.carts.length > 0){
    html += '<div class="form-container btn-input col-md-12"><input class="btn btn-primary" type="button" value="Checkout" id="checkout_button" onclick="checkout_process();" /></div>';
  }
  html += '<div class="form-container col-md-12"><table class="cart_table">';
  html += '<thead><tr><td>No.</td><td>Card Name</td><td>Qty</td><td>Subtotal</td></tr></thead>';
  
  if(my_account.data.carts.length > 0){
    var carts = my_account.data.carts;
    var carts_count = carts.length;
    for(var a = 0; a < carts_count; a++){
      html += '<tbody><tr><td>'+(a + 1)+'</td><td>'+carts[a].name+'<br/>'+carts[a].sku+' - '+carts[a].rarity+'</td><td>'+carts[a].qty+'</td><td>'+carts[a].subtotal+'</td></tr></tbody>';
    }
    html += '<tbody><tr><td colspan="3">Ongkos Kirim : </td><td>'+carts[0].shipping_fee+'</td></tr></tbody>';
    html += '<tbody><tr><td colspan="3">Total : </td><td>'+carts[0].total_price+'</td></tr></tbody>';
  } else {
    html += '<tbody><tr><td colspan="4">Tidak ada barang dalam Cart</td></tr></tbody>';
  }

  html += '</table></div>';
  html += '<div class="form-container btn-input col-md-12"></div>';
  html += '</form>';
  html += '</div>';

  cities = get_cities();
  cities_combobox = '<select class="form-control city_field" name="city" readonly>';
  for(var a = 0; a < cities.length; a++){
    var city = cities[a];
    if(my_account.data.account.city_id == city.id){
      cities_combobox += '<option value="'+city.id+'" selected="selected">'+city.name+'</option>';
    } else {
      cities_combobox += '<option value="'+city.id+'">'+city.name+'</option>';
    }
    
  }
  cities_combobox += '</select>';

  //My Account
  html += '<div class="my_account">';
  html += '<form action="#" id="cart">';
  html += '<div class="form-container col-md-12 my_account_title"><h4>Akun</h1></div>';
  html += '<div class="form-container btn-input col-md-12"><input class="btn btn-primary" id="update_account_click" type="submit" value="Ubah Akun" onclick="update_account_form(); return false;" /> <input class="btn btn-primary" type="button" value="Logout" id="logout_button" onclick="logout();" /></div>';
  html += '<div class="form-container col-md-12">Username</div><div class="form-container col-md-12"><input type="text" class="form-control username_field" name="username" value="'+my_account.data.account.username+'" readonly /></div>';
  html += '<div class="form-container col-md-12">Password</div><div class="form-container col-md-12"><input type="password" class="form-control password_field" name="password" value="'+my_account.data.account.password+'" readonly /></div>';
  html += '<div class="form-container col-md-12">Full Name</div><div class="form-container col-md-12"><input type="text" class="form-control fullname_field" name="fullname" value="'+my_account.data.account.fullname+'" readonly /></div>';
  html += '<div class="form-container col-md-12">Email</div><div class="form-container col-md-12"><input type="email" class="form-control email_field" name="email" value="'+my_account.data.account.email+'" readonly /></div>';
  html += '<div class="form-container col-md-12">Phone</div><div class="form-container col-md-12"><input type="text" class="form-control phone_field" name="phone" value="'+my_account.data.account.phone+'" readonly /></div>';
  html += '<div class="form-container col-md-12">Address</div><div class="form-container col-md-12"><textarea class="form-control address_field" name="address" readonly >'+my_account.data.account.address+'</textarea></div>';
  html += '<div class="form-container col-md-12">Kota</div><div class="form-container col-md-12">'+cities_combobox+'</div>';
  html += '</form>';
  html += '</div>';

  $(".modal").html(html);
  $(".modal").modal();

  $('#my-account').submit(function(event){
    $.post("home/update_account", $(this).serialize(), function( data ) {
      var output = JSON.parse(data);
      alert(output.message);
      document.location="";
    });
    return false;
  });
  return false;
}

function update_account_form(){
  if($("#update_account_click").val() == "Ubah Akun"){
    prepare_update();
  } else {
    submit_update();
    $.modal.close();
  }

  return false;
}

function prepare_update(){
  $(".password_field").prop("readonly", false);
  $(".fullname_field").prop("readonly", false);
  $(".email_field").prop("readonly", false);
  $(".phone_field").prop("readonly", false);
  $(".address_field").prop("readonly", false);
  $(".city_field").prop("readonly", false);
  $("#update_account_click").val("Konfirmasi Perubahan");
}

function submit_update(data){

  var data = {
    password : $(".password_field").val(),
    fullname : $(".fullname_field").val(),
    email : $(".email_field").val(),
    phone : $(".phone_field").val(),
    address : $(".address_field").val(),
    address : $(".city_field").val()
  };
  var responseText = $.ajax({url: "home/update_account", type: 'POST', async : false, data}).responseText;
  var response = JSON.parse(responseText);

  if(response.code == "SUCCESS"){
    alert("Akun berhasil diubah");
  } else {
    alert(response.message);
  }

  $(".password_field").prop("readonly", true);
  $(".fullname_field").prop("readonly", true);
  $(".email_field").prop("readonly", true);
  $(".phone_field").prop("readonly", true);
  $(".address_field").prop("readonly", true);
  $(".city_field").prop("readonly", true);
  $("#update_account_click").val("Ubah Akun");
}

function transaction(){
  this.blur(); // Manually remove focus from clicked link.

  var responseText = $.ajax({url: "transaction", async : false}).responseText;
  var transactions = JSON.parse(responseText);
  if(transactions.code == "ERROR"){
    alert(transactions.message);
    document.location = "";
    return false;
  }

  var html = "";

  //Waiting For Payment
  html += '<div class="cart">';
  html += '<form action="#" id="my-account">';
  html += '<div class="form-container col-md-12 cart_title"><h4>Daftar Transaksi</h1></div>';
  html += '<div class="form-container col-md-12"><table class="cart_table">';
  html += '<thead><tr><td>No.</td><td>Tanggal Transaksi</td><td>Total Harga</td><td>Status - Detail</td></tr></thead>';
  if(transactions.data.transactions.length > 0){
      var transactions = transactions.data.transactions;
      var transactions_count = transactions.length;
      for(var a = 0; a < transactions_count; a++){
        html += '<tbody><tr><td>'+transactions[a].id+'</td><td>'+transactions[a].created_datetime+'</td><td>'+transactions[a].total_price+'</td><td>'+transactions[a].transaction_status +'</td></tr></tbody>';
      }
  } else {
    html += '<tbody><tr><td colspan="4">Tidak ada transaksi saat ini</td></tr></tbody>';
  }

  html += '</table></div>';
  html += '<div class="form-container btn-input col-md-12"></div>';
  html += '</form>';
  html += '</div>';

  $(".modal").html(html);
  $(".modal").modal();

  return false;
}

function contact_us(){
  this.blur(); // Manually remove focus from clicked link.

  var html = "";

  html += '<div class="cart">';
  html += '<div class="form-container col-md-12"><h4>Hubungi Kami</h4></div>';
  html += '<div class="form-container col-md-12" style="padding:20px; text-align:center; font-size:16px;">';
  html += '<h5>Yu-Gi-Oh! Shop Official</h5>';
  html += '<ul style="text-align:left;">';
  html += '<li>Email : <a href="mailto:yosia@socianovation.com">yosia@socianovation.com</a></li>';
  html += '<li>Whatsapp : <a href="https://wa.me/6283890252590" target="_blank">+6283890252590</a></li>';
  html += '</ul>';
  html += '</div>';
  html += '</div>';
  $(".modal").html(html);
  $(".modal").modal();

  return false;
}

function how_to_buy(){
  this.blur(); // Manually remove focus from clicked link.

  var html = "";

  html += '<div class="cart">';
  html += '<div class="form-container col-md-12"><h4>Cara Bertransaksi</h4></div>';
  html += '<div class="form-container col-md-12" style="padding:20px; text-align:center; font-size:16px;">';
  html += '<ol style="text-align:left;">';
  html += '<li>Login dengan menggunakan akun anda</li>';
  html += '<li>Bila belum punya akun, bisa lakukan registrasi melalui link <a href="">ini</a></li>';
  html += '<li>Cari dan pilih kartu yang anda inginkan, cari menggunakan textbox "Search" di bagian kanan atas</li>';
  html += '<li>Pastikan kode booster dan rarity kartu sudah sesuai dengan cara klik "Lihat Detil"</li>';
  html += '<li>Setelah dipastikan, klik "Tambah ke Cart"</li>';
  html += '<li>Ketik Qty yang diinginkan, lalu tekan submit</li>';
  html += '<li>Ulangi langkah di atas, sampai anda selesai menambahkan kartu-kartu yang anda inginkan</li>';
  html += '<li>Klik menu yang bertuliskan username anda</li>';
  html += '<li>Klik tombol Checkout, dan konfirmasi</li>';
  html += '<li>Lakukan pembayaran ke rekening BCA 0845274709 a/n Yosia, sesuai dengan nominal total</li>';
  html += '<li>Setelah lakukan pembayaran, klik lagi menu yang bertuliskan username anda</li>';
  html += '<li>Klik konfirmasi pembayaran dan konfirmasi, setelah ini anda cukup menunggu konfirmasi pengiriman barang dari pihak YGOShop</li>';
  html += '<li>Pihak YGOShop akan memverifikasi pembayaran anda maksimal 1x24 jam, sekaligus melakukan konfirmasi pengiriman</li>';
  html += '</ol>';
  html += '</div>';
  html += '</div>';
  $(".modal").html(html);
  $(".modal").modal();

  return false;
}

function about(){
  this.blur(); // Manually remove focus from clicked link.

  var html = "";

  html += '<div class="cart">';
  html += '<div class="form-container col-md-12"><h4>Tentang</h4></div>';
  html += '<div class="form-container col-md-12" style="padding:20px; text-align:center; font-size:16px;">'
  html += 'YGOShop adalah platform jual beli kartu Yu-Gi-Oh! TCG Original, kami menjamin semua barang yang dijual di website ini adalah kartu asli.';
  html += '</div>';
  html += '</div>';
  $(".modal").html(html);
  $(".modal").modal();

  return false;
}

function search_process(start = "", limit = ""){
  $("#product_list").html("");
  $.ajax({url: "product/list" + "?search=" + $("#search_field").val() + "&start=" + start + "&limit=" + limit, success: function(result){
    var output = "";
    // var product_output = JSON.parse(result).data;
		var product_output = result.data;
    var product_array = product_output.list;
    var product_page = product_output.pages;

    var count = product_array.length;
    var pages_count = product_page.length;
    var product_per_page = 12;

    for(var a = 0; a < count; a++){
      var product = product_array[a];
      output += '<div class="card-square col-md-3 float-left"><div class="card-container"><a href="product/detail/'+product.product_id+'" class="view_detail" product="'+product.product_id+'"><div class="card-body"><h5 class="card-title">'+product.name+'</h5><h6 class="card-subtitle text-muted">'+product.rarity+' - '+product.sku+'</h6></div><img class="card-image" src="'+product.image+'" alt="Card image"><ul class="list-group list-group-flush"><li class="list-group-item">'+money_format(product.product_currency, product.product_price)+'</li>';
      if(product.qty > 0){
          output += '<li class="list-group-item">Stock : '+product.qty+'</li>';
      } else {
          output += '<li class="list-group-item">Stock : <span class="text-red">Out of Stock</span></li>';
      }
      output += '</ul></a><div class="card-body"><a href="product/detail/'+product.product_id+'" type="button" class="btn btn-info add_to_cart" product="'+product.product_id+'">Tambah ke Cart</a></div></div></div>';
    }

    var paging = "";
    var max_pagination = 10;
    if(start > 0){
      paging += '<li class="page-item"><a class="page-link" href="#" onclick="search_process('+(start - 12)+','+limit+')">Previous</a></li>';
    }
    for(var a = 0; a < pages_count; a++){
      if(a >= max_pagination + start) {
        break;
      }

      if(a * product_per_page == start){
        paging += '<li class="page-item"><a class="page-link"';
        paging += 'style="background-color:#808080;"';
        paging += ' href="#" onclick="search_process('+product_page[a].start+','+product_page[a].limit+')">'+ (a+1) +'</a></li>';
      } else {
        var page_position = start / product_per_page
        var min_page_position = page_position - 1 == 0 ? 0 : page_position - 1;
        var max_page_position = page_position + 1 > pages_count ? pages_count : page_position + 1;
        if(a > min_page_position || a < max_page_position) {
          paging += '<li class="page-item"><a class="page-link"';
          paging += ' href="#" onclick="search_process('+product_page[a].start+','+product_page[a].limit+')">'+ (a+1) +'</a></li>';
        }
      }
    }

    paging += '<li class="page-item"><a class="page-link" href="#" onclick="search_process('+(start + product_per_page)+','+limit+')">Next</a></li>';

    $("#product_list").html(output);

    $("#product_paging").html(paging);

    $('.view_detail').click(function(event) {
      event.preventDefault();
      this.blur(); // Manually remove focus from clicked link.
      $.get(this.href, function(result) {
        // $(html).appendTo('body').modal();
        var product = JSON.parse(result).data;

        var html = "";
        html += '<div class="card-detail"><div class="card-name">'+product.name+'</div><div class="card-image"><img src="assets/images/no_image.jpeg" data-src="'+product.image+'"</div><div class="card-description">'+product.description+'</div><div class="card-attributes">';

        html += '</div></div>';

        $(".modal").html(html);
        $(".modal").modal({fadeDuration: 100});

      });
    });

    $('.add_to_cart').click(function(event) {
      event.preventDefault();
      this.blur(); // Manually remove focus from clicked link.
      $.get(this.href, function(result) {
        // $(html).appendTo('body').modal();
        var product = JSON.parse(result).data;

        var html = "";
        var product_id = product.product_id;
        html += '<form action="#" id="form'+product_id+'">';
        html += '<div class="card-detail"><div class="card-name">'+product.name+'</div>';
        html += '<div class="card-description"><span class="card-how-many">How many?</span><br/><input type="hidden" name="product_id" value="'+product_id+'" /><input class="form-control card-qty" type="text" name="qty" /><br/><input type="submit" class="btn btn-primary" value="Submit"></div>';
        html += '</div>';
        html += '</form>';

        $(".modal").html(html);
        $(".modal").modal();

        $('#form'+product_id).submit(function(event){
          $.post("cart/add_to_cart", $(this).serialize(), function( data ) {
            var output = JSON.parse(data);
            alert(output.message);
            $.modal.close();
          });
          return false;
        });
      });
    });

    $('#view_cart').click(function(event) {
      event.preventDefault();
      this.blur(); // Manually remove focus from clicked link.
      $.get("cart/list", function(result) {
        var cart = JSON.parse(result).data;

        var html = "";
        html += '<div class="card-detail"><div class="card-name">'+product.name+'</div><div class="card-image"><img src="'+product.image+'"</div><div class="card-description">'+product.description+'</div><div class="card-attributes">';

        html += '</div></div>';

        $(".modal").html(html);
        $(".modal").modal();
      });
    });

    $('#login').click(function(event) {
      check_session();
    });

    $(document).trigger("ajaxComplete");

  }});
  return false;
}

$(document).ready(function(){
  var session = check_session();
  if(session != false){
    var my_account = '<a class="nav-link" href="#" id="my_account" onclick="my_account()">'+session+'</a>';
    var transaction = '<a class="nav-link" href="#" id="transaction" onclick="transaction()">My Transactions</a>';
    $("#login_section").html(my_account);
    $("#transaction_section").html(transaction);
  }
  search_process();
});

$(document).ajaxComplete(function() {
  $(".card-image img").each(function() {
    // Check if the image is already loaded (might be from cache)
    if (this.complete) {
      $(this).attr("src", $(this).data("src")); // Replace directly
    } else {
      // Preload the image (if it's not from cache)
      var img = new Image();
      img.src = $(this).data("src");

      $(img).on("load", function() {
        $(this).hide(); // Hide before replacement (optional)
        $(this).attr("src", this.src).fadeIn(); // Replace and fade in (optional)
      });
    }
  });
});